const express = require('express')
const router = express.Router()
const comments = require('../../controllers/comment.js')

module.exports = () => {

  router.get('/', comments.filter)
  
  return router

}