const express = require('express')
const router = express.Router()
const photos = require('../../controllers/photo.js')

module.exports = () => {

  router.get('/', photos.findAll)
  router.get('/:id', photos.findOne)
  router.get('/user/:userId', photos.findByUser)

  return router

}