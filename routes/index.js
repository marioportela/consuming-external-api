const express = require('express')

const router = express.Router()

const user = require('./user')
const album = require('./album')
const photo = require('./photo')
const comment = require('./comment')

module.exports = () => {

  router.get('/', async (req, res) => {
    res.send('Api is Ok...')
  })

  router.use('/users', user())
  router.use('/albums', album())
  router.use('/photos', photo())
  router.use('/comments', comment())
  
  return router
}