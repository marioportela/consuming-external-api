module.exports = class CustomError {
    constructor(e){
      let errorCode = null
      let msg = ''

      switch (e.name) {
        case 'notFound':
          errorCode = 0
          msg = e.msg
          break
        case 'exists':
          errorCode = 1
          msg = e.msg
          break
        case 'notValid':
          errorCode = 2
          msg = e.msg
          break
        case 'ValidationError':
          errorCode = 3
          msg = 'Error de validación'
          var errors = []
          let keys = Object.keys(e.errors)
          for (let key of keys) {
            errors.push({key: e.errors[key].path, msg: e.errors[key].message})
          }
          break
        case 'CastError':
          errorCode = 4
          msg = 'ObjectId incorrecto'
          break
        case 'MongoError':
          errorCode = e.code
          msg = 'Ya existe un registro con esta información'
          break
        case 'error':
          errorCode = 5
          msg = e.msg
          break
        case 'notAllow':
          errorCode = 6
          msg = e.msg
      }

      switch(errorCode) {
        case 0: // Recurso no encontrado
          this.msg = msg
          this.statusCode = 404
          this.type = 'Recurso no encontrado'
          break
        case 1: // Recurso ya existe
          this.msg = msg
          this.statusCode = 409
          this.type = 'Recurso existente'
          break
        case 2: // Recurso no válido
          this.msg = msg
          this.statusCode = 400
          this.type = 'Recurso no válido'
          break
        case 3: // Error de validación
          this.msg = msg
          this.errors = errors
          this.statusCode = 400
          this.type = 'Validación fallida'
          break
        case 4: // Error de casteo
          this.msg = msg
          this.statusCode = 500
          this.type = 'Error casting'
          break
        case 5: // Error de sintaxis
          this.msg = msg
          this.statusCode = 400
          this.type = 'Sintaxis mala'
          break
        case 6: // Error de permiso
          this.msg = msg
          this.statusCode = 403
          this.type = 'No autorizado'
          break
        case 11000: // Recurso duplicado Mongo
          this.msg = msg
          this.statusCode = 409
          this.type = 'Recurso existente'
          break
        default:
          this.msg = 'Error desconocido'
          this.statusCode = 500
          this.type = 'Desconocido'
          this.error = e.toString()
      }
    }
}
