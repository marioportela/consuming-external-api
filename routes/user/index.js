const express = require('express')
const router = express.Router()
const users = require('../../controllers/user.js')

module.exports = () => {

  router.get('/', users.findAll)
  router.get('/:id', users.findOne)

  return router

}