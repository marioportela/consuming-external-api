const axios = require('axios')

exports.findAll = async (req, res) => {
    axios.get('https://jsonplaceholder.typicode.com/photos')
      .then(response => {
        if (response.status == 200) {
          res.send(response.data)
        } else {
          res.send('error')
        }
      })
      .catch(e => console.log(e))
  }

exports.findOne = async (req, res) => {
    let id = req.params.id
    axios.get(`https://jsonplaceholder.typicode.com/photos/${id}`)
      .then(response => {
        if (response.status == 200) {
          res.send(response.data)
        } else {
          res.send('error')
        }
      })
      .catch(e => console.log(e))
  }

exports.findByUser = async (req, res) => {
    let userId = req.params.userId
    axios.get('https://jsonplaceholder.typicode.com/photos')
      .then(response => {
        if (response.status == 200) {
          let photos = response.data
          axios.get(`https://jsonplaceholder.typicode.com/albums?userId=${userId}`)
            .then(response => {
              if (response.status == 200) {
                let albums = response.data
                let albumsIds = []
                let photosByuserId = []
                
                for (let album in albums){
                  albumsIds.push(albums[album].id)
                }
                for (let j = 0; j < photos.length; j++ )
                {
                  let isPhoto = albumsIds.indexOf(photos[j].albumId)
                  if (isPhoto !== -1) photosByuserId.push(photos[j])
                }
                res.send(photosByuserId)
              } else {
                res.send('error')
              }
            })
            .catch(e => console.log(e))
        } else {
          res.send('error')
        }
      })
      .catch(e => console.log(e))
  }