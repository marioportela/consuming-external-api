const config = require('./config.js')

// Dependencies
let mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// connection to MongoDB
mongoose.connect(`mongodb://${config.url}:${config.port}/${config.database}`, {useNewUrlParser: true })
    .catch(err => console.log(err));

mongoose.connection.on('connected', () => {
  console.log(`[Mongoose]: Connection open to ${config.url}/${config.database}`)
})
mongoose.connection.on('error', (e) => {
  console.log(`[Mongoose]: Connection error: ${e}`)
})
mongoose.connection.on('disconnected', () => {
  console.log('[Mongoose]: Connection disconnected')
})
