const axios = require('axios')

exports.findAll = async (req, res) => {
    axios.get('https://jsonplaceholder.typicode.com/users')
  		.then(response => {
  			if (response.status == 200) {
  				res.send(response.data)
  			} else {
  				res.send('error')
  			}
  		})
  		.catch(e => console.log(e))
  }

exports.findOne = async (req, res) => {
  	let id = req.params.id
    axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
  		.then(response => {
  			if (response.status == 200) {
  				res.send(response.data)
  			} else {
  				res.send('error')
  			}
  		})
  		.catch(e => console.log(e))
  }