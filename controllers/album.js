const axios = require('axios')
const customError = require('../errors/customError.js')

const Album = require('../models/album.js')

exports.findAll = async (req, res) => {
    axios.get('https://jsonplaceholder.typicode.com/albums')
      .then(response => {
        if (response.status == 200) {
          res.send(response.data)
        } else {
          res.send('error')
        }
      })
      .catch(e => console.log(e))
  }

exports.findOne = async (req, res) => {
    let id = req.params.id
    axios.get(`https://jsonplaceholder.typicode.com/albums/${id}`)
      .then(response => {
        if (response.status == 200) {
          res.send(response.data)
        } else {
          res.send('error')
        }
      })
      .catch(e => console.log(e))
  }

exports.findByUser = async (req, res) => {
    let userId = req.params.userId
    axios.get(`https://jsonplaceholder.typicode.com/albums?userId=${userId}`)
      .then(response => {
        if (response.status == 200) {
          res.send(response.data)
        } else {
          res.send('error')
        }
      })
      .catch(e => console.log(e))
  }

exports.shareAlbum = async (req, res) => {
    // Request validation
    try {
      if(req.body === null) {
          throw {
            name: 'notValid', 
            msg: 'Debe enviar información del album a compartir'
          }
      }

      // Create a shared album
      const album = new Album({
          albumId: req.body.albumId, 
          userId: req.body.userId,
          permit: req.body.permit
      })

      // Save shared album in the database
      album.save()
      .then(data => {
          res.send(data)
      }).catch(err => {
          res.status(500).send({
              message: err.message || "Something wrong while sharing the album."
          })
      })
    } catch (e) {
      throw new CustomError(e)
    }
    
  }

exports.changePermits = async (req, res) => {
    let userId = req.params.userId
    let albumId = req.params.albumId

    try {
      Album.findOne({
        userId: userId, 
        albumId: albumId
      })
      .then(album => {
          if(!album) {
            return res.status(404).send({
                message: 'album not found'
            })
          }
          let data = {permit:req.body.permit}
          let id = album._id
          
          Album.findOneAndUpdate({_id: id}, {$set: data}, {useFindAndModify: false, runValidators: true})
          .then(updatedAlbum => {
            updatedAlbum.permit = req.body.permit
            res.send(updatedAlbum)  
          })
          .catch(e => {
            return res.status(500).send({
              message: `Something wrong updating permit: ${e}`
            })

          })
          
      }).catch(err => {
          return res.status(500).send({
              message: 'Something wrong updating permit--'
          })
      })
    } catch (e) {
      return res.status(500).send({
              message: 'Something wrong updating permit'
          })
    }

  }

exports.findUsersByPermit = async (req, res) => {
    let albumId = req.params.albumId
    let permit = req.params.permit

    Album.find({
      albumId: albumId,
      permit: permit
    })
    .then(albums => {
      let users = []
      for(let i = 0; i < albums.length; i++) {
        users.push(albums[i].userId)
      }
        res.send(users)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Something wrong while retrieving users permits"
        })
    });
  
  }
  