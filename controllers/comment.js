const axios = require('axios')

exports.filter = async (req, res) => {
   let name = req.query.name
  
   if (!name) {
    res.status(500).send({
        message: "Debe especificar el name del comentario"
    })
   } else {
    axios.get(`https://jsonplaceholder.typicode.com/comments?name=${name}`)
      .then(response => {
        if (response.status == 200) {
          res.send(response.data)
        } else {
          res.status(500).send({
            message: err.message || "Something wrong while retrieving comments"
        })
        }
      })
      .catch(e => console.log(e))
   }
  }
