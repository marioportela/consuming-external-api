const mongoose = require('mongoose')

let permits = {
  values: ['read', 'write'],
  message: 'permiso no válido'
}

const AlbumSchema = mongoose.Schema({
    albumId: {type: Number, required: true },
    userId: {type: String, required: true },
    permit: {type: String, required: true, enum: permits}
}, {
    timestamps: true
});

module.exports = mongoose.model('Albums', AlbumSchema);