const express = require('express')
const router = express.Router()
const albums = require('../../controllers/album.js')

module.exports = () => {

  router.get('/', albums.findAll)
  router.get('/:id', albums.findOne)
  router.get('/user/:userId', albums.findByUser)
  router.get('/users/:albumId/:permit', albums.findUsersByPermit)
  router.post('/share-album', albums.shareAlbum)
  router.patch('/change-permit/:albumId/:userId', albums.changePermits)

  return router

}