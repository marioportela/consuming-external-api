const config = require('./config')

const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

require('./db')

const routes = require('./routes')

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(routes())


app.listen(config.port, () => {
    console.log(`Api listening at port ${config.port}`)
});